package nautilus.game.dominate.stats;

public class DominatePlayerStats
{
  public int Points;
  public int Kills;
  public int Deaths;
  public int Assists;
}
