package nautilus.game.dominate.stats;

import java.util.List;

public class DominateGameStatsToken
{
  public long Duration;
  public List<DominatePlayerStatsToken> PlayerStats;
}
