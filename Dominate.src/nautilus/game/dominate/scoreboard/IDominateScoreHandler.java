package nautilus.game.dominate.scoreboard;

import nautilus.game.core.scoreboard.ITeamScoreHandler;
import nautilus.game.dominate.engine.IDominateTeam;
import nautilus.game.dominate.player.IDominatePlayer;

public abstract interface IDominateScoreHandler
  extends ITeamScoreHandler<IDominatePlayer, IDominateTeam>
{}
