package nautilus.game.dominate.engine;

public abstract interface IPowerUp
{
  public abstract void Update();
  
  public abstract void Deactivate();
}
