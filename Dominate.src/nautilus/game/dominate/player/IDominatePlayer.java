package nautilus.game.dominate.player;

import nautilus.game.core.player.ITeamGamePlayer;
import nautilus.game.dominate.engine.IDominateTeam;

public abstract interface IDominatePlayer
  extends ITeamGamePlayer<IDominateTeam>
{}
